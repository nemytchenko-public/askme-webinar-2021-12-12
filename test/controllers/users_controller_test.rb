require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "#create" do
    params = {
      :email => 'jopa@test.com',
      :password => 'testtest',
      :password_confirmation => 'testtest',
      :name => 'Aboba',
      :username => 'aboba887',
      :avatar_url => 'http://ya.ru',
      :color => '#224422'
    }

    post users_path, params: { user: params }

    @user = User.last

    assert_response :redirect
    assert_equal @user.username, 'aboba887'
    assert_equal @user.name, 'Aboba'
  end
end
