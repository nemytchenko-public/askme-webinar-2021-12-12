require "test_helper"

class QuestionsControllerTest < ActionDispatch::IntegrationTest
  # test "#create" do
    # params = {
    #   :email => 'jopa@test.com',
    #   :password => 'testtest',
    #   :password_confirmation => 'testtest',
    #   :name => 'Aboba',
    #   :username => 'aboba887',
    #   :avatar_url => 'http://ya.ru',
    #   :color => '#224422'
    # }

  #   post users_path, params: { user: params }

  #   @user = User.last

  #   assert_response :redirect
  #   assert_equal @user.username, 'aboba887'
  #   assert_equal @user.name, 'Aboba'
  # end

  test "#create when not logged in" do
    @user = User.create(
      :email => 'jopa@test.com',
      :password => 'testtest',
      :password_confirmation => 'testtest',
      :name => 'Aboba',
      :username => 'aboba887',
      :avatar_url => 'http://ya.ru',
      :color => '#224422'
    )

    params = {
      :user_id => @user.id,
      :text => 'Как дела?'
    }

    post questions_path, params: { question: params }

    @question = @user.questions.last
    assert_response :redirect

    assert @question.text == 'Как дела?'
  end

  test "#create when logged in" do
    @existing_user = users(:existing)
    @our_user = users(:our)

    login(@our_user)

    params = {
      :user_id => @existing_user.id,
      :text => 'Как дела?'
    }

    post questions_path, params: { question: params }

    @question = Question.last
    assert_response :redirect

    assert { @question.text == 'Как дела?' }
    assert { @question.author_id == @our_user.id }
    assert { @existing_user == @question.user }
  end
end
